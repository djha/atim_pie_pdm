<?php
error_reporting(0);
include_once 'database_connection.php';
include_once './atim_pdm/data.php';
include "./atim_pdm/polygon/polygon.php";
include_once "./atim_pdm/colorchip/ColorChip.class.php";

$form_id = $_POST["form_id"];

$column = $_POST["column"];
$value = $_POST["value"];
$from_date = date("Y/m/d", strtotime($_POST["from"]));
$to_date = date("Y/m/d", strtotime($_POST["to"]));

$lat_long = lat_long_fields($form_id);
$xml = simplexml_load_file('./atim_pdm/OxnardOnly1.kml') or die("could not load");
$ns = $xml->getDocNamespaces();
if(isset($ns[""])){
	$xml->registerXPathNamespace("ge",$ns[""]);
}

$allValues = all_values($form_id, $column, $value, $from_date, $to_date);

$polygons = array();
foreach ($xml->xpath('//ge:Placemark') as $placemark) {
	$simpleDatas = $placemark->ExtendedData->SchemaData->SimpleData;

	foreach($placemark->ExtendedData->SchemaData->SimpleData as $valuea) {
		foreach($valuea->attributes() as $key=>$val){
			if((string)trim($val) === "AREA") {
				$area = floatval($valuea);
			}
		}
	}
	
	$polygon = $placemark->Polygon;

	$outerCoors = explode(" ", $polygon->outerBoundaryIs->LinearRing->coordinates);
	$innerCoors = explode(" ", $polygon->innerBoundaryIs->LinearRing->coordinates);
	

//Modified code
	$polyOuter = new polygon();
	$polyInner = new polygon();        // Create a new polygon and add some vertices to it
	
	
	foreach($outerCoors as $outerCoor) {
		$outerCoor = trim($outerCoor);
		$xy = explode(",", $outerCoor);
		$polyOuter->addv(floatval($xy[0]), floatval($xy[1]));
	}
	
	foreach($innerCoors as $innerCoor) {
		$innerCoor = trim($innerCoor);
		$xy = explode(",", $innerCoor);
		$polyInner->addv(floatval($xy[0]), floatval($xy[1]));
	}
	$polygons[(string)$placemark->name]	= 0;
	
	foreach ($allValues as $key=>$val){
		//Modified code
		$testPoly = new polygon();
		$testPoly->addv($val["lat"], $val["long"]);
		$c =& $testPoly->first;
		
		if ($polyOuter->isInside($c) && (!$polyInner->isInside($c))) {
			$polygons[(string)$placemark->name]++;
			unset($allValues[$key]);	
		}
	}
	$polygons[(string)$placemark->name] /= $area;

}


$total = array_sum($polygons);

//Placed in another loop since color requires total count
foreach ($xml->xpath('//ge:Placemark') as $placemark) {
	
	$simpleData = $placemark->ExtendedData->SchemaData->addChild("SimpleData", $polygons[(string)$placemark->name]);
	$simpleData->addAttribute("name", "Density");
	if($polygons[(string)$placemark->name] != 0) {
		$color = 255*(1 - $polygons[(string)$placemark->name]/$total);
		$placemark->Style->PolyStyle->addChild('color', "FF" .fromRGB($color, $color, $color));
		$placemark->Style->PolyStyle->fill = 1;
		
		$placemark->Polygon->addChild('extrude','1');
		$placemark->Polygon->addChild('altitudeMode','relativeToGround');
		
	}
	else 
		$placemark->Style->PolyStyle->addChild('color', "ff3f3f3f");
}

	header('Content-type: "text/kml"; charset="utf8"');
	header('Content-disposition: attachment; filename="' . $column . '_' . $value . '.kml"');

echo $xml->asXML();
?>



