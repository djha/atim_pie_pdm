<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <?php
include_once "data.php";
 ?>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script> -->
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery-ui-1.8.20.custom.min.js"></script>
<!-- <script src="js/bootstrap-datepicker.js"></script> -->
<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/datepicker.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/redmond/jquery-ui-1.8.20.custom.css" />  
<link rel="stylesheet" href="css/jquery.multiselect.css" />
<!-- <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" /> -->

<style type="text/css">
 .ui-corner-all input {
 	display: inline;
 	margin-top: 0px;
 } 
 
 .container {
 	margin-top: 45px;
 }
 
 h2 {
 	margin: 0px 0px 15px 70px;
 	//border-bottom: 1px black solid;
 }
 
 #search {
 	border-top: 1px solid #DDDDDD;
 	padding-top: 30px;
 }
 </style>

<script>
	$(function() {
		populateColumns();
	    $('#form_fields').change(populateColumns);
	    $("#columns").change(populateValues);
	   $('.datepicker').datepicker({ numberOfMonths: 3, 
		   changeMonth: true,
	        changeYear: true,
	        defaultDate: '-2m',
	        yearRange: '2008:'
			   });
	   $('#form_fields option:selected').removeAttr('selected');
	   //$("#form_fields").multiselect({noneSelectedText: 'Select Form(s)'});
	   //$("#columns").multiselect({noneSelectedText: 'Select Column(s)'});
	   $("#popup").hide();
	   // updateTextArea();
	  /* $('#search').submit(function() {
			//location.reload();
		   $.getJSON('process.php?'+$(this).serialize(), function(data) {
			  //alert(data.count);
			  //alert(data.columns);
			  $("#inner").empty();
			   $.each(data, function(key, val) {
				   var date = new Date();
				   var time = date.getTime();
				    //alert(key + ";" + val);
				    $("#inner").append("<div class='accordion-group'><div class='accordion-heading'>" +  
			        "<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2' href='#collapse" + key + "'>" +
			        val + 
			        "</a></div>" +
			      "<div id='collapse" + key + "' class='accordion-body in collapse' style='height: auto; '>" + 
			        "<div class='accordion-inner'>" + 
			        "<img src='graph/pie-chart.php?data=graph_config/data" + key + ".txt&config=graph_config/config" + key + ".txt&random='" + time + " width=400 height=300 /></div></div>");
			   });
			   //$("#popup").show();
			   $("#popup").dialog({width: 500});
			   $('.accordion-group .accordion-heading').click(function() {
					$(this).next().toggle();
					return false;
				});

		   });
		   return false;
	   });*/
	});
			  /*$.each(data, function(key, val) {
			    items.push('<li id="' + key + '">' + val + '</li>');
			  });
			
			  $('<ul/>', {
			    'class': 'my-new-list',
			    html: items.join('')
			  }).appendTo('body');*/
			
		
	   /*$('#search').submit(function() {
		   features = 'width=400,height=400,toolbar=no,location=no,directories=no,menubar=no,scrollbars=no,copyhistory=no,resizable=no';
		   window.open("", $(this).target + $(this).serialize(),features);
		   return true;
		 });*/
	
   	function populateColumns() {
    	 $("#columns").html("");
         var allVals = [];
         $('#form_fields :checked').each(function() {
           allVals.push($(this).val());
         });
         //alert(allVals);
         $.getJSON("populate_columns.php", {'form_ids[]': allVals, 'columns': true}, function(data) {
        	    //alert("JSON Data: " + data.columns);
        	    $.each(data.columns, function(index, value) { 
            	    //alert(value);
            	    $("#columns").append("<option  class='clearfix' value='" + value + "'> " + value + "</option>");
        	    	//$("#columns").append("<label  class='checkbox'><input type='checkbox' name='columns[]' value='" + value + "'/> " + value + "</label>");
        	    }); 
        	    populateValues();
       	 });
      }

	  function populateValues() {
		  $("#values").html("");
		  var column = $("#columns").val();
		  var form_id = $("#form_fields").val();
		  $.getJSON("populate_columns.php", {'column': column, 'form_id': form_id, 'values': true}, function(data) {
      	    //alert("JSON Data: " + data.columns);
      	    $.each(data.values, function(index, value) { 
          	    $("#values").append("<option  class='clearfix' value='" + value + "'> " + value + "</option>");
      	    }); 
     	 });
	  } 

    
</script>

<title></title>
</head>
<body>
<div class="container">
<div class="well">
<h2>Search</h2>
<form class="form-horizontal" action="extractCoordinates.php" method="post" id="search" accept-charset="UTF-8">
                <fieldset>
                    <div class="control-group check_boxes">
                        <label for="form_fields" class="check_boxes control-label">Form Field</label>
                        <div class="controls scrollable">
<!--                          <ul class="nav nav-list"> -->
<!--                          <ul class="inputs-list"> -->
                        <select  id="form_fields" name="form_id" class="clearfix">
<?php
	$result = form_fields();
	while ($row = $result->fetch_assoc()) {
		echo "<option  class='clearfix' value='" . $row['id'] . "'> " . $row['form_id'] . "</option>";
// 		echo "<label class='checkbox'><input type='checkbox' name='form_field[]' class='check_boxes' value='"
// 				. $row['id'] . "'/> " . $row['form_id'] . "</label>";
	}
	date_default_timezone_set('UTC');
	$currDate = date("m/d/Y");
	
 ?>
 </select>
<!--                             </ul>    -->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="columns" class="control-label">Column</label>
                        <div class="controls scrollable">
                        <select  name="column" id="columns">
                        </select>
<!--                          <ul class="nav nav-list"> -->
<!--                          <ul class="inputs-list"> -->
                         
<!--                            </ul>    -->
                        </div>
                    </div>
                    
                     <div class="control-group">
                        <label for="value" class="control-label">Value</label>
                        <div class="controls scrollable">
                        <select  name="value" id="values">
                        </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
						<label class="control-label" for="from">From</label>
					 	<div class="controls" id="columns">
			            	<input class="span2 datepicker" id="from" type="text" name="from" value="">
			            </div>
					</div>
					
                    <div class="control-group">
						<label class="control-label" for="to">To</label>
					 	<div class="controls" id="columns">
			            	<input class="span2 datepicker" id="to" type="text" name="to" value="">
			            </div>
					</div>
					
	  	

                    
                    <div class="form-actions">
                          <input type="submit" class="btn btn-primary" value="Run Search" name="submit"/>
<!-- 								<button class="btn">Cancel</button> -->
							</div>
                    

                </fieldset>
            </form>
            </div>
            </div>
 
<div id="popup" title="Pie Chart"> 
<div id="inner"></div>
</div>      
    
</body>
</html>