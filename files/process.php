<?php

include_once "./files/database_connection.php";
include_once "./colorchip/ColorChip.class.php";

error_reporting(E_ALL & ~E_NOTICE);

error_log("inside process" . $_GET["from"]);
// error_log(var_dump($_GET));
// error_log(var_dump($_POST));
$form_ids = $_GET["form_ids"];
// var_dump($form_ids);
$columns = $_GET["columns"];
$from_date = date("Y/m/d", strtotime($_GET["from"]));
$to_date = date("Y/m/d", strtotime($_GET["to"]));

$aocv = array(); //array of column values
$columnNames = array(); //array of column names

$dbconn = connect();
$sql = "select xml_string from assets_view where created >= '$from_date' and created <= '$to_date' and form_name in (" . implode(',', $form_ids) . ")";
// echo $sql;
// echo $sql;
$result = $dbconn->query($sql);
while ($row = $result->fetch_assoc()) {
	$xml = new SimpleXMLElement($row["xml_string"]) or die("could not parse");
	
// 	$xml = simplexml_load_string($row["xml_string"]);
// 	if (!$xml) {
// 		echo "Failed loading XML\n";
// 		foreach(libxml_get_errors() as $error) {
// 			echo "fff";
// 			echo "\t", $error->message;
// 		}
// 	}
	
	foreach ($columns as $column) {
		$col_value = trim($xml->instance->data->$column); //$column = Ph , $col_value=Ph1
		if (isset($col_value) && $col_value != "") {
			$aocv[$column][$col_value]++; //$aocv["Ph"]["Ph1"]
		}
	}

}

// var_dump($aocv);

$colors = array("8888CC", "22CC33", "CC2233", "FF8800", "CC00CC", "0088FF");

$chartCount = 1;
foreach ($aocv as $column => $value_pairs) {
	
	$columnNames[$chartCount] = $column;

	// 	echo "column:" . $column;  //Ph
	$sliceCount = 1;

	$myFile = "graph/graph_config/data" . $chartCount . ".txt";
	$fh = fopen($myFile, 'w') or die("could not open");

	copy("graph/graph_config/config_template.txt",
	"graph/graph_config/config" . $chartCount . ".txt");
	$myFile1 = "graph/graph_config/config" . $chartCount . ".txt";
	$fh1 = fopen($myFile1, 'a');

	$stringData = "text: " . $column . "|#555555|200,300|arial|12|true|false|0"
	. "\n";
	fwrite($fh1, $stringData);

	$total_slice = sizeof($value_pairs);
	$step = 360/$total_slice;
	$white = 0xFFFFFF;
	$colorDec = intval($white / $total_slice);
	$baseColor = base_convert($colorDec, 10, 16);
	// 	$color = new ColorChip($baseColor, null, null, CC_HEX);
	$color = new ColorChip("cc2233", null, null, CC_HEX);
	foreach ($value_pairs as $data => $count) {
		$stringData = "segment" . strval($sliceCount) . ": #"
		. $color->hex . "|" . $data . "|\n";
		fwrite($fh1, $stringData);

		$stringData = "data" . strval($sliceCount) . "series1" . ":   " . $count . "\n";
		// 		echo $stringData;
		fwrite($fh, $stringData);
		$sliceCount++;
		$color->adjHue($step);
	}

	$chartCount++;
	fclose($fh);
	fclose($fh1);

}

echo json_encode($columnNames);

?>

