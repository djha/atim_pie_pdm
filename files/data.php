<?php
include_once 'database_connection.php';

function form_fields() {
	$dbconn = connect();
	$sql = "select id, form_id from dl_forms order by form_id";
	$result = $dbconn->query($sql);
	$dbconn->close();
	return $result;
}

//could not use fields column of dl_forms because we need to find distinct and order alphabetically
function column_names($form_ids) {
	$dbconn = connect();
	$ids_string = implode(",", $form_ids);
	$sql = "select distinct column_name from dl_forms_display_template where form_id in ($ids_string) order by column_name";
	error_log($sql);
	$result = $dbconn->query($sql);
	error_log($dbconn->error);
	$column_names = array();
	while($row = $result->fetch_assoc()) {
		array_push($column_names, $row["column_name"]);
	}
	error_log(implode(",", $column_names));
	return $column_names;
}

//using fields column of dl_forms
// function column_names($form_ids) {
// 	$dbconn = connect();
// 	$ids_string = implode(",", $form_ids);
// 	$sql = "select fields from dl_forms where id in ($ids_string)";
// 	error_log($sql);
// 	$result = $dbconn->query($sql);
// 	error_log($dbconn->error);
// 	$column_names = array();
// 	while($row = $result->fetch_assoc()) {
// // 		error_log($row["fields"]);
// // 		$cnames = explode("|", $row["fields"]);
// // 		$cnames = explode("|", "device_id|subscriber_id|sim_id|AlarmStatus|Location|GPS|MGRS|Latitude|Longitude|LocationText|Form_Version|FusionBlog|Photo");
// // 		error_log(implode(",", $cnames));
// 		$column_names = array_merge($column_names, explode("|", $row["fields"]));
// 	}
// 	error_log(implode(",", $column_names));	
// 	return $column_names;
// }
