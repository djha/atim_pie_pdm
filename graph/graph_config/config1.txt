 width                =440
 height               =300
 backgroundColor      =#f9f5da
 ndecplaces           =0
 legendPadding        =6
 legendRoundRadius    =3
 legendOpacity        =1.0
 legendBackground     =true
 legendBackgoundColor ='#ffffff'
 legendBorderColor    ='#888'
 legendBorderWidth    =1
 legendStyle          ='vertical'
 legendXpos           =300
 legendYpos           =20
 
legendTitleFontFamily:         Arial
legendTitleFontSize:     10
legendFontStyle:     bold
legendfontitalic:   true

titlefontFamily =          Arial
titleFontSize =      12
titleFontStyle =      bold
titleFontColor=         #444444
titleposition:      5,60
 series1 = cx: 150; cy: 150; r: 60; title: Ph
legendLabels = Ph1:#cc2233;Ph2:#33CC22;Ph3:#2233CC;