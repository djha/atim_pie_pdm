<?php
//include_once 'database_connection.php';
include_once "database_connection.php";
include_once "colorchip/ColorChip.class.php";


class Form_Data extends CI_Model{

function _construct()
{
	parent::_construct();
}


 function all_xml_strings($form_ids, $from_date, $to_date, $columns) {                                           
                                                                                        
                $column_values = array();                                               
                $sql = "select xml_string from assets_view where created >= '$from_date' and created <= '$to_date' and form_name in (" . implode(',', $form_ids) . ")";
                // echo $sql;                                                           
                // echo $sql;                                                           
                $result = $this->db->query($sql);                                       
                foreach ($result->result() as $row) {                                 
                        $xml = new SimpleXMLElement($row->xml_string) or die("could not parse");
                                                                                        
                        foreach ($columns as $column) {                                 
                                if(!isset($column_values[$column]))                     
                                        $column_values[$column] = array();              
                                $col_value = (string)trim($xml->instance->data->$column); //$column = Ph , $col_value=Ph1
                                if (isset($col_value) && $col_value != "") {            
                                        if(isset($column_values[$column][$col_value])) 
                                                $column_values[$column][$col_value]++; 
                                        else                                            
                                                $column_values[$column][$col_value] = 1;
                                }                                                       
                        }                                                               
                }                                                                       
                                                                                        
                return $column_values;                                                  
        }          
function form_fields() {
	
	//New in CodeIgniter form
	//$data=array(new array());
	$sql="select id,form_id from dl_forms order by form_id";
	$Q=$this->db->query($sql);
	
	echo 'No of rows '.$Q->num_rows().' <br/>';
	/*foreach($Q->result_array() as $row)
	{
		//echo $row['id'];
		//echo $row['form_id'].' <br/>';
		$data[$row['id']]=$row['form_id'];
		//$data=$row;
	}*/
	
	//return  $data;
	return $Q;//->result_array();
	
	
	//Code Previously in CodeIgniter form
	
	/*$CI =& get_instance();
    $CI->load->model('database_connection');

    // use $CI instead of $this to query the other models
    $dbconn=$CI->database_connection->connect();
		
	//$dbconn = connect();
	$sql = "select id, form_id from dl_forms order by form_id";
	$result = $dbconn->query($sql);
	
	$dbconn->close();
	
	
			
	return $result;*/
}


//Use the database connection parameter and rerurns the selected value from the 
//existing database

//These values will be returned to the controller 
//Controller will eventually at last will provide the value to
//the view

function database_datas()
{

	$CI =& get_instance();
    $CI->load->model('database_connection');

    // use $CI instead of $this to query the other models
    $dbconn=$CI->database_connection->connect();
	
	
	$from_date = date("Y/m/d", strtotime($_GET["from"]));
	$to_date = date("Y/m/d", strtotime($_GET["to"]));
	
	//$dbconn = connect();
	$sql = "select xml_string from assets_view where created >= '$from_date' and created <= '$to_date' and form_name in (" . implode(',', $form_ids) . ")";
	$result = $dbconn->query($sql);
	$dbconn->close();
			
	return $result;
}


//could not use fields column of dl_forms because we need to find distinct and order alphabetically
function column_names($form_ids) {

//Also in CodeIgniter standard format
	/*$CI =& get_instance();
    $CI->load->model('database_connection',true);

    // use $CI instead of $this to query the other models
    $CI->database_connection->connect();
	$dbconn = $CI->database_connection->connect();
	
	//$dbconn = connect();
	$ids_string = implode(",", $form_ids);
	$sql = "select distinct column_name from dl_forms_display_template where form_id in ($ids_string) order by column_name";
	error_log($sql);
	$result = $dbconn->query($sql);
	error_log($dbconn->error);
	$column_names = array();
	while($row = $result->fetch_assoc()) {
		array_push($column_names, $row["column_name"]);
	}
	error_log(implode(",", $column_names));
	return $column_names;*/
	
	
	//In CodeIgniter format
	
	$ids_string = implode(",", $form_ids);
	$sql = "select distinct column_name from dl_forms_display_template where form_id in ($ids_string) order by column_name";
	error_log($sql);
	$result = $this->db->query($sql);
	//error_log($this->db->error);
	$column_names = array();
	
	while($row = $result->fetch_assoc()) {
		array_push($column_names, $row["column_name"]);
	}
	error_log(implode(",", $column_names));
	return $column_names;
}
}
//using fields column of dl_forms
// function column_names($form_ids) {
// 	$dbconn = connect();
// 	$ids_string = implode(",", $form_ids);
// 	$sql = "select fields from dl_forms where id in ($ids_string)";
// 	error_log($sql);
// 	$result = $dbconn->query($sql);
// 	error_log($dbconn->error);
// 	$column_names = array();
// 	while($row = $result->fetch_assoc()) {
// // 		error_log($row["fields"]);
// // 		$cnames = explode("|", $row["fields"]);
// // 		$cnames = explode("|", "device_id|subscriber_id|sim_id|AlarmStatus|Location|GPS|MGRS|Latitude|Longitude|LocationText|Form_Version|FusionBlog|Photo");
// // 		error_log(implode(",", $cnames));
// 		$column_names = array_merge($column_names, explode("|", $row["fields"]));
// 	}
// 	error_log(implode(",", $column_names));	
// 	return $column_names;
// }
