<?php
include_once "database_connection.php";
include_once "colorchip/ColorChip.class.php";


class Process extends CI_Model{

function _construct()
{
	parent::_construct();
}


function get_Column_Name($formids,$columns,$from_date,$to_date) {

$aocv = array(); //array of column values
$columnNames = array(); //array of column names

$sql = "select xml_string from assets_view where created >= '$from_date' and created <= '$to_date' and form_name in (" . implode(',', $form_ids) . ")";
$result=$this->db->query($sql);
//$result = $dbconn->query($sql);

while ($row = $result->fetch_assoc()) {
	$xml = new SimpleXMLElement($row["xml_string"]) or die("could not parse");
	
	
	foreach ($columns as $column) {
		$col_value = trim($xml->instance->data->$column); //$column = Ph , $col_value=Ph1
		if (isset($col_value) && $col_value != "") {
			$aocv[$column][$col_value]++; 
		}
	}

}

$colors = array("8888CC", "22CC33", "CC2233", "FF8800", "CC00CC", "0088FF");

$chartCount = 1;
foreach ($aocv as $column => $value_pairs) {
	
	$columnNames[$chartCount] = $column;
	$sliceCount = 1;

	$myFile = "graph/graph_config/data" . $chartCount . ".txt";
	$fh = fopen($myFile, 'w') or die("could not open");

	copy("graph/graph_config/config_template.txt",
	"graph/graph_config/config" . $chartCount . ".txt");
	$myFile1 = "graph/graph_config/config" . $chartCount . ".txt";
	$fh1 = fopen($myFile1, 'a');

	$stringData = "text: " . $column . "|#555555|200,300|arial|12|true|false|0"
	. "\n";
	fwrite($fh1, $stringData);

	$total_slice = sizeof($value_pairs);
	$step = 360/$total_slice;
	$white = 0xFFFFFF;
	$colorDec = intval($white / $total_slice);
	$baseColor = base_convert($colorDec, 10, 16);
	// 	$color = new ColorChip($baseColor, null, null, CC_HEX);
	$color = new ColorChip("cc2233", null, null, CC_HEX);
	
	$temp=1;
	foreach ($value_pairs as $data => $count) {
		$stringData = "segment" . strval($sliceCount) . ": #"
		. $color->hex . "|" . $data . "|\n";
		fwrite($fh1, $stringData);

		
		
	if($temp==1)
		$stringData = '{'. '"data' . strval($sliceCount) . 'series1' . '":   " '. $count .'"';

		//For every sequence in the file, put , at the first 
		else if($temp>1)
			$stringData = ','. '"data' . strval($sliceCount) . 'series1' . '":   " '. $count .'"';

				$temp++;
		// 		echo $stringData;
		fwrite($fh, $stringData);
		$sliceCount++;
		$color->adjHue($step);
	}
	
	//To close the JSON data file 
	$stringData = '}';
	fwrite($fh,$stringData);
	
	$chartCount++;
	fclose($fh);
	fclose($fh1);

}

return json_encode($columnNames);

}
}
?>