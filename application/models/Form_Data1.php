<?php
include_once 'database_connection.php';


class Form_data1 extends CI_Model
{
	function _construct()
	{
		parent::_construct();
	}

	
	function all_xml_strings($form_ids, $from_date, $to_date, $columns) {                                           
                                                                                        
                $column_values = array();                                               
                $sql = "select xml_string from assets_view where created >= '$from_date' and created <= '$to_date' and form_name in (" . implode(',', $form_ids) . ")";
                // echo $sql;                                                           
                // echo $sql;                                                           
                $result = $this->db->query($sql);                                       
                foreach ($result->result() as $row) {                                 
                        $xml = new SimpleXMLElement($row->xml_string) or die("could not parse");
                                                                                        
                        foreach ($columns as $column) {                                 
                                if(!isset($column_values[$column]))                     
                                        $column_values[$column] = array();              
                                $col_value = (string)trim($xml->instance->data->$column); //$column = Ph , $col_value=Ph1
                                if (isset($col_value) && $col_value != "") {            
                                        if(isset($column_values[$column][$col_value])) 
                                                $column_values[$column][$col_value]++; 
                                        else                                            
                                                $column_values[$column][$col_value] = 1;
                                }                                                       
                        }                                                               
                }                                                                       
                                                                                        
                return $column_values;                                                  
        }          

	function form_fields() {
		/*$dbconn = connect();
		$sql = "select id, form_id from dl_forms order by form_id";
		$result = $dbconn->query($sql);
		$dbconn->close();
		return $result;*/
		
		//My codeigniter code
	$sql="select id,form_id from dl_forms order by form_id";
	$Q=$this->db->query($sql);
	
	//echo 'No of rows '.$Q->num_rows().' <br/>';
	
	return $Q;
	}

	//could not use fields column of dl_forms because we need to find distinct and order alphabetically
	function column_names($form_ids) {
			
		$ids_string = implode(",", $form_ids);
		$sql = "select distinct column_name from dl_forms_display_template where form_id in ($ids_string) order by column_name";
		error_log($sql);
		$result = $this->db->query($sql);
		//error_log($this->db->error);
		$column_names = array();
		
		foreach($result->result() as $row) {
			array_push($column_names, $row->column_name);
		}
		error_log(implode(",", $column_names));
		return $column_names;

	}

	function lat_long_fields($form_id) {
		
		$query = "select manual_coordinates_latitude, manual_coordinates_longitude from dl_form_mappings where form_id = $form_id";
		$result = $this->db->query($query);
		$row = $result->fetch_assoc();
		return $row;
		
	}

	function unique_values($form_id, $column) {
		$uniques = array();
		
		$sql = "select xml_string from assets_view where form_name = $form_id";
		$result = $this->db->query($sql);
		foreach ($result->result() as $row) {
			$xml = new SimpleXMLElement($row->xml_string) or die("could not parse");
			$col_value = trim($xml->instance->data->$column); //$column = Ph , $col_value=Ph1
			array_push($uniques, $col_value);
		}
		$uniques = array_unique($uniques);
		return $uniques;
		
	}

	function all_values($form_id, $column, $value, $from_date, $to_date) {
		
		$lat_long = lat_long_fields($form_id);
		$allValues = array();
		//$dbconn = connect();
		$sql = "select xml_string from assets_view where created >= '$from_date' and created <= '$to_date' and  form_name = $form_id";
		$result = $this->db->query($sql);
		while ($row = $result->fetch_assoc()) {
			$xml = new SimpleXMLElement($row["xml_string"]) or die("could not parse");
			$col_value = trim($xml->instance->data->$column); //$column = Ph , $col_value=Ph1
			$lat = trim($xml->instance->data->$lat_long["manual_coordinates_latitude"]);
			$long = trim($xml->instance->data->$lat_long["manual_coordinates_longitude"]);
			if($col_value === $value)
				array_push($allValues, array('lat'=>floatval($lat), 'long'=>floatval($long)));
		}
		return $allValues;
	}

	function fromRGB($R, $G, $B){
		$R=dechex($R);
		if (strlen($R)<2)
			$R='0'.$R;
		$G=dechex($G);
		if (strlen($G)<2)
			$G='0'.$G;
		$B=dechex($B);
		if (strlen($B)<2)
			$B='0'.$B;
		return '' . $R . $G . $B;
	}
}