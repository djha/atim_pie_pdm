<?php
error_reporting(0);
include_once 'database_connection.php';
include_once 'data.php';
include "polygon/polygon.php";
include_once "colorchip/ColorChip.class.php";

// $form_id = $_GET["form_id"];
// // var_dump($form_ids);
// $column = $_GET["column"];
// $from_date = date("Y/m/d", strtotime($_GET["from"]));
// $to_date = date("Y/m/d", strtotime($_GET["to"]));
// // var_dump($_POST);

$form_id = $_POST["form_id"];
// var_dump($form_ids);
$column = $_POST["column"];
$value = $_POST["value"];
$from_date = date("Y/m/d", strtotime($_POST["from"]));
$to_date = date("Y/m/d", strtotime($_POST["to"]));

$lat_long = lat_long_fields($form_id);
// echo $lat_long["manual_coordinates_latitude"] . "\n";
// echo $lat_long["manual_coordinates_longitude"];

// $xml = simplexml_load_file('test.kml') or die("could not load");
$xml = simplexml_load_file('OxnardOnly1.kml') or die("could not load");
$ns = $xml->getDocNamespaces();
if(isset($ns[""])){
	$xml->registerXPathNamespace("ge",$ns[""]);
}
// print_r($xml);

$allValues = all_values($form_id, $column, $value, $from_date, $to_date);
// var_dump($allValues);

$polygons = array();
foreach ($xml->xpath('//ge:Placemark') as $placemark) {
//     echo "outer: " . $polygon->outerBoundaryIs->LinearRing->coordinates;
//     echo "\n inner:" . $polygon->outerBoundaryIs->LinearRing->coordinates;
// 	$polygons[$placemark->name]	= array("outer"=> array(), "inner"=>array());
	$simpleDatas = $placemark->ExtendedData->SchemaData->SimpleData;
// 	var_dump($placemark->ExtendedData->SchemaData->SimpleData);

// 	echo $placemark->ExtendedData->SchemaData->SimpleData[1];

	
// 	foreach($simpleDatas as $simpleD){
// 		foreach($simpleD->attributes as $key=>$value){
// // 			echo $value;
// 			if($value === "AREA") {
// 				$area = floatval($simpleD);
// 				echo $simpleD;
// 			}
// 		}	
// 	}

	foreach($placemark->ExtendedData->SchemaData->SimpleData as $valuea) {
		foreach($valuea->attributes() as $key=>$val){
			// 			echo $value;
			// 			echo "  " . $key . ": " . $val . "  ";
			if((string)trim($val) === "AREA") {
				$area = floatval($valuea);
// 								echo $value;
			}
		}
		// 		echo $value . "  ";
	}
	
	$polygon = $placemark->Polygon;
// 	echo $polygon;
	$outerCoors = explode(" ", $polygon->outerBoundaryIs->LinearRing->coordinates);
	$innerCoors = explode(" ", $polygon->innerBoundaryIs->LinearRing->coordinates);
	
// 	echo "outer" . $polygon->outerBoundaryIs->LinearRing->coordinates;

	$polyOuter =& new polygon();
	$polyInner =& new polygon();        // Create a new polygon and add some vertices to it
	
// 	var_dump($outerCoors);
	
	foreach($outerCoors as $outerCoor) {
		$outerCoor = trim($outerCoor);
// 		echo $outerCoor;
		$xy = explode(",", $outerCoor);
		$polyOuter->addv(floatval($xy[0]), floatval($xy[1]));
	}
	
	foreach($innerCoors as $innerCoor) {
		$innerCoor = trim($innerCoor);
// 				echo $innerCoor;
		$xy = explode(",", $innerCoor);
		$polyInner->addv(floatval($xy[0]), floatval($xy[1]));
	}
// 	$polygons[$placemark->name]	= array("outer"=> $polyOuter, "inner"=>$polyInner);
	
// 	echo((string)$placemark->name);
	$polygons[(string)$placemark->name]	= 0;
	
	foreach ($allValues as $key=>$val){
// 		var_dump($val);
		
		$testPoly =& new polygon();
		// $testPoly->addv(4,130);
		$testPoly->addv($val["lat"], $val["long"]);
// 		$testPoly->addv(-120.22, 35.138);
// 		$testPoly->addv(-120.28, 35.098);
		$c =& $testPoly->first;
// 		var_dump($c);
// 		var_dump($polyOuter);
		
// 		echo $polyOuter->isInside($c)? "true" : "false";
// 		echo $polyInner->isInside($c)? "true" : "false";
// // 		echo $polyOuter->isInside($c);
// 		echo sizeof($allValues);
		
		if ($polyOuter->isInside($c) && (!$polyInner->isInside($c))) {
// 			echo "inside";
			$polygons[(string)$placemark->name]++;
			unset($allValues[$key]);	
		}
// 		var_dump($c);
// 		echo $placemark->name . ": " . $polygons[(string)$placemark->name];
	}
	$polygons[(string)$placemark->name] /= $area;
// 	var_dump($allValues);
}

// var_dump($polygons);

$total = array_sum($polygons);
// $black = 0x000000;
// $colorDec = intval($black / $total_slice);
// $baseColor = base_convert($colorDec, 10, 16);
// 	$color = new ColorChip($baseColor, null, null, CC_HEX);

//Placed in another loop since color requires total count
foreach ($xml->xpath('//ge:Placemark') as $placemark) {
	
// 	$color = new ColorChip("D1C7C7", null, null, CC_HEX);
// 	$color->adjHue(360/$polygons[(string)$placemark->name]);
// 	echo " " . $color->hex . " ";
// 	$area = floatVal($placemark->xpath('//ExtendedData[SchemaData[SimpleData[@name="AREA"]]]'));
// 	var_dump($placemark->xpath('//ExtendedData[SchemaData[SimpleData[@name="AREA"]]]'));
	$simpleData = $placemark->ExtendedData->SchemaData->addChild("SimpleData", $polygons[(string)$placemark->name]);
	$simpleData->addAttribute("name", "Density");
	if($polygons[(string)$placemark->name] != 0) {
// 		$black = 255;
		$color = 255*(1 - $polygons[(string)$placemark->name]/$total);
// 		echo $step;
// 		$color->adjHue($step);
		$placemark->Style->PolyStyle->addChild('color', "FF" .fromRGB($color, $color, $color));
// 		echo  'color', "FF" .fromRGB($color, $color, $color) . "    ";
		$placemark->Style->PolyStyle->fill = 1;
		$placemark->Polygon->addChild('extrude','1');
		$placemark->Polygon->addChild('altitudeMode','relativeToGround');
		
	}
	else 
		$placemark->Style->PolyStyle->addChild('color', "ff3f3f3f");
}

	header('Content-type: "text/kml"; charset="utf8"');
	header('Content-disposition: attachment; filename="' . $column . '_' . $value . '.kml"');

echo $xml->asXML();

// foreach ($polygons as $name => $count) {

// $movies = simplexml_load_file('test.kml') or die("could not load");
// // $xml = simplexml_load_file($file);
// $ns = $movies->getDocNamespaces();
// if(isset($ns[""])){
//  	$movies->registerXPathNamespace("default",$ns[""]);
// }

// foreach ($movies->xpath('//default:character') as $character) {
// 	print_r($character);
// 	echo $character->name, 'played by ', $character->actor, PHP_EOL;
// }


// print_r($xml->Document->Folder);

// echo $xml->Document->Folder->name;

/*$xmlstr = <<<XML
<?xml version='1.0' standalone='yes'?>
<movies>
 <movie>
  <title>PHP: Behind the Parser</title>
  <characters>
   <character>
    <name>Ms. Coder</name>
    <actor>Onlivia Actora</actor>
   </character>
   <character>
    <name>Mr. Coder</name>
    <actor>El Act&#211;r</actor>
   </character>
  </characters>
  <plot>
   So, this language. It's like, a programming language. Or is it a
   scripting language? All is revealed in this thrilling horror spoof
   of a documentary.
  </plot>
  <great-lines>
   <line>PHP solves all my web problems</line>
  </great-lines>
  <rating type="thumbs">7</rating>
  <rating type="stars">5</rating>
 </movie>
</movies>
XML;

$movies = new SimpleXMLElement($xmlstr);

foreach ($movies->xpath('//character') as $character) {
	print_r($character);
	echo $character->name, 'played by ', $character->actor, PHP_EOL;
}

foreach ($movies->xpath('//name') as $name) {
	echo $name, PHP_EOL;
}
*/


