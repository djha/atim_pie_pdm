/* --------------------------------------------------------------------------------------------- */
/*                                                                                               */
/* For the full range of options and parameter meanings please see:-                             */
/*                                                                                               */
/* http://www.jpowered.com/php-scripts/adv-graph-chart/documentation/configurationOptions.htm    */
/*                                                                                               */
/* --------------------------------------------------------------------------------------------- */

<!-- Chart Characteristics -->
width:              400
height:             300
ndecplaces:         0
pecentndecplaces:   0
depth3d:            20
3dangle:            40

backgroundcolor:    #ffffff

<!-- Chart Switches -->
3d:                 true
displayPercentages: true
labellines:         true
quality:            very high

<!-- Popup segment Value Pre & Post Symbols -->
<!--valuepresym: $ -->

<!-- thousand seperater -->
thousandseparator: ,

<!-- Segment Labels -->
segmentlabels:            true
segmentlabelfont:         Arial
segmentlabelfontsize:     8
segmentlabelfontbold:     false
segmentlabelfontitalic:   false
segmentlabelcolor:        #666666

<!-- Pie Data --> 
<!--                x,y|size|seperation -->
pie1:               190,220|120|5
pie2:               480,310|140|5

<!-- Title --> 
<!--titletext:          Sales by Region-->
titlefont:          Arial
titlefontsize:      12
titlefontbold:      true
titlefontitalic:    false
titlecolor:         #444444
titleposition:      5,60

<!-- Legend Information -->
legend:             true
legendfont:         Arial
legendfontsize:     10
legendfontbold:     false
legendfontitalic:   true
legendposition:     5,70
<!-- legendtitle:        Sales Region -->
legendbgcolor:      #FFFFFF
legendbordercolor:  #DDDDDD
legendtextcolor:    #202020
legendstyle:        horizontal


