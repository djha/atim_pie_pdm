<?php
	
	class Base extends CI_Controller
	{
		function Base()
		{
			parent::__construct();
		}

		function populate() {
			$form_ids = $this->input->get("form_ids", TRUE);
			$column_names = $this->Form_Data1->column_names($form_ids);
			echo json_encode(array('columns' => $column_names));
		}

		
		function process() {                                                         
                                                                                     
                $form_ids = $this->input->get("form_ids", TRUE);                     
                // var_dump($form_ids);                                              
                $columns = $this->input->get("columns",TRUE);                        
                $from_date = date("Y/m/d", strtotime($this->input->get("from",       
                                                                    TRUE)));         
                $to_date = date("Y/m/d", strtotime($this->input->get("to",           
                                                                    TRUE)));         
                                                                                     
                // echo(array_push($aocv, "fruit"=>"mango"));                        
                // var_dump($aocv);                                                  
                $columnNames = array(); //array of column names                      
                                                                                     
                $aocv = $this->Form_Data->all_xml_strings($form_ids, $from_date, $to_date, $columns);                    
                                                                                     
                //var_dump($aocv);                                                   
                                                                                     
                $colors = array("8888CC", "22CC33", "CC2233", "FF8800", "CC00CC", "0088FF");
                                                                                     
                // $chartCount = time();                                             
                $chartCount = 1;                                                     
                foreach ($aocv as $column => $value_pairs) {                         
                                                                                     
                        $columnNames[$chartCount] = $column;                         
                                                                                     
                        //  echo "column:" . $column;  //Ph                          
                        $sliceCount = 1;                                             
                                                                                     
                        $dataFile = "graph/graph_config/data" . $chartCount . ".txt";  
                        $dataHndlr = fopen($dataFile, 'w') or die("could not open"); 
                                                                                     
                        copy("graph/graph_config/config_template.txt", "graph/graph_config/config" . $chartCount . ".txt");
                        $configFile = "graph/graph_config/config" . $chartCount . ".txt";
                        $configHndlr = fopen($configFile, 'a');                      
                                                                                     
                        $stringData = " series1 = cx: 150; cy: 150; r: 60; title: " . $column . "\n";
                        fwrite($configHndlr, $stringData);                           
                                                                                     
                        $total_slice = sizeof($value_pairs);                         
                        $step = 360/$total_slice;                                    
                        $white = 0xFFFFFF;                                           
                        $colorDec = intval($white / $total_slice);                   
                        $baseColor = base_convert($colorDec, 10, 16);                
                        //  $color = new ColorChip($baseColor, null, null, CC_HEX);  
                        $color = new ColorChip("cc2233", null, null, CC_HEX);        
                        $stringData = "legendLabels = ";                           

                       fwrite($configHndlr, $stringData);                           
                                                                                     
                        foreach ($value_pairs as $data => $count) {                  
                                $colorString = "#" . $color->hex;                    
                                                                                     
                                $stringData = "segment" . strval($sliceCount) . ": #" . $color->hex . "|" . $data . "|\n";
                                $stringData = $data . ":" . $colorString . ";"; //Product A : #88c;
                                                                                     
                                fwrite($configHndlr, $stringData);                   
                                                                                     
                                $stringData = "series1" . "data" . strval($sliceCount) . "=   " . $count . "; color: " . $colorString . ";\n";  //series1data1=   1;color:red;
                                //      echo $stringData;                            
                                fwrite($dataHndlr, $stringData);                     
                                $sliceCount++;                                       
                                $color->adjHue($step);                               
                        }                                                            
                                                                                     
                        $chartCount++;                                               
                        fclose($dataHndlr);                                          
                        fclose($configHndlr);                                        
                                                                                     
                }                                                                    
                                                                                     
                //$data['json'] = json_encode($columnNames);
				
				//$this->load->vars($data);
				echo json_encode($columnNames);
                //$this->load->view('json_view', $data);                                      
                                                                                     
        }                            
		function Get_Process_Data()
		{
		
			if(isset($_POST['form_id']))
				$formids=$_POST['form_id'];		 
		 	if($_POST['columns'])
				$columns=$_POST['columns'];		 
			if($_POST['from_date'])
				$columns=$_POST['from_date'];		 
			if($_POST['to_date'])
				$columns=$_POST['to_date'];		 
				
		 		$result = $this->Process->get_Column_Name($formids,$columns,$from_date,$to_date); 
				
				json_encode($result);
		
		}
		
		function populate_columns()
		{
			if($this->input->get("columns", TRUE) === 'true') {
				$form_ids = $this->input->get("form_ids", TRUE);
				
				$form_id_result=$this->Form_Data1->column_names($form_ids) ;
								
				echo json_encode(array('columns'=>$form_id_result));
			} else if($this->input->get("values", TRUE) === 'true') {
				$column = $this->input->get("column", TRUE);
				$form_id = $this->input->get("form_id", TRUE);
				
				$value_result=$this->Form_Data1->unique_values($form_id, $column);
				//print_r(unique_values($form_id, $column)));
				echo json_encode(array('values'=>$value_result));
				
			}
		}
		function index()
		{
			$data['title']="PHP to CodeIgniter conversion";
				//$this->load->helper('form');
						
			//I have changed here for the demo for the ATIM_PDM
			//Get the database form_ ids from the database
			$result = $this->Form_Data1->form_fields();
			
			//We r formatting to result array not the object
			$result=$result->result_array();				
			$data['form_id']=$result;
						
			$this->load->vars($data);
			$this->load->view('Atim_Pie_View');
						
			
    	}
		
		function pdm()
		{
			$data['title']="PHP to CodeIgniter conversion";
				//$this->load->helper('form');
						
			//I have changed here for the demo for the ATIM_PDM
			//Get the database form_ ids from the database
			$result = $this->Form_Data1->form_fields();
			
			//We r formatting to result array not the object
			$result=$result->result_array();				
			$data['form_id']=$result;
					
			
			$this->load->vars($data);
					
			$this->load->view('pdmbase_view');
						
			
    	}
		
	}
?>

