<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery-ui-1.8.20.custom.min.js"></script>

<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
<script src="js/activeCharts/js/jpgraph.js" type="text/javascript" charset="utf-8"></script>
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/datepicker.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/redmond/jquery-ui-1.8.20.custom.css" />  
<link rel="stylesheet" href="css/jquery.multiselect.css" />

<style type="text/css">
 .ui-corner-all input {
 	display: inline;
 	margin-top: 0px;
 } 
 
 .containerCustom {
 	margin-top: 45px;
 }
 
 h2 {
 	margin: 0px 0px 15px 70px;
 	//border-bottom: 1px black solid;
 }
 
 #search {
 	border-top: 1px solid #DDDDDD;
 	padding-top: 30px;
 }
 </style>

 <script>
	$(document).ready(function() {
	    $('#form_fields').change(populateColumns);
	   $('.datepicker').datepicker({ numberOfMonths: 3, 
		   changeMonth: true,
	        changeYear: true,
	        defaultDate: '-2m',
	        yearRange: '2008:'
			   });
	   $('#form_fields option:selected').removeAttr('selected');
	   $("#form_fields").multiselect({noneSelectedText: 'Select Form(s)'});
	   $("#columns").multiselect({noneSelectedText: 'Select Column(s)'});
	   $("#popup").hide();
	   //$("#graph").hide();
	   
	   // updateTextArea();
	   $('#search').submit(function() {
		   $.getJSON('index.php/pie/process?'+$(this).serialize(), function(data) {
			  //alert(data.count);
			  //alert(data.columns);
			  $("#inner").empty();
			   $.each(data, function(key, val) {
				    //alert(key + ";" + val);
				    $("#inner").append("<div class='accordion-group'><div class='accordion-heading'>" +  
			        "<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2' href='#collapse" + key + "'>" +
			        val + 
			        "</a></div>" +
			        "<div id='collapse" + key + "' class='accordion-body in collapse' style='height: auto;'>" + 
			        "<div class='accordion-inner'>" + 
					"<div  id='chart" + key + "'>" + 
			        "</div></div></div>");
					//alert('pie_chart_files/data' + key + '.txt');
					new JpGraph(
						'pie',
						'chart' + key,
						null,
						{
							JpGraphPropertyFile : 'graph/graph_config/config' + key + '.txt',
							JpGraphDataFile : 'graph/graph_config/data' + key + '.txt'
					});
			   });
			   $("#popup").dialog({width: 500});
			   $('.accordion-group .accordion-heading').click(function() {
					$(this).next().toggle();
					return false;
				});
			  
			   
		   });
		   return false;
	   });
	});
			  
	
     function populateColumns() {
    	 $("#columns").html("");
         var allVals = [];
         $('#form_fields :checked').each(function() {
           allVals.push($(this).val());
         });
         //alert(allVals);
         $.getJSON("./index.php/pie/populate", {'form_ids[]': allVals}, function(data) {
        	    //alert("JSON Data: " + data.columns);
        	    $.each(data.columns, function(index, value) { 
            	    //alert(value);
            	    $("#columns").append("<option  class='clearfix' value='" + value + "'> " + value + "</option>");
        	    	//$("#columns").append("<label  class='checkbox'><input type='checkbox' name='columns[]' value='" + value + "'/> " + value + "</label>");
        	    }); 
        	    $("#columns").multiselect("refresh");
       	 });
      }

    
</script>

 
</head>
<title>ATIM</title>

<body>
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse"> <span class="icon-bar"></span>
				<span class="icon-bar"></span><span class="icon-bar"></span>
			<a class="brand" href="./">ATIM</a>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<li class=""><a href="./">Pie Charts</a>
					</li>
				</ul>
				<ul class="nav">
					<li class=""><a href="./index.php/pdmview">Point Density Map</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>	
<div class="container containerCustom">
<div class="well">
<h2>Begin Chart Query</h2>
<form class="form-horizontal" action="" method="post" id="search">
                <fieldset>
                    <div class="control-group check_boxes">
                        <label for="form_fields" class="check_boxes control-label">Form Fields</label>
                        <div class="controls scrollable">
                        <select  id="form_fields" name="form_ids[]" multiple="multiple" class="multiple clearfix">
<?php


	$result = $form_id;
	foreach($result as $row){
		echo "<option  class='clearfix' value='" . $row['id'] . "'> " . $row['form_id'] . "</option>";
	}
	
	
	date_default_timezone_set('UTC');
	$currDate = date("m/d/Y");
	
 ?>
 </select>
<!--                             </ul>    -->
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="columns" class="control-label">Columns</label>
                        <div class="controls scrollable">
                        <select  name="columns[]" multiple="multiple" class="multiple clearfix" id="columns">
                        </select>
                         
                        </div>
                    </div>
                    
                    <div class="control-group">
						<label class="control-label" for="from">From</label>
					 	<div class="controls" id="columns">
			            	<input class="span2 datepicker" id="from" type="text" name="from" value="">
			            </div>
					</div>
					
                    <div class="control-group">
						<label class="control-label" for="to">To</label>
					 	<div class="controls" id="columns">
			            	<input class="span2 datepicker" id="to" type="text" name="to" value="">
			            </div>
					</div>
					
	  	

                    
                    <div class="form-actions">
                          <input type="submit" class="btn btn-primary" value="Run Search" name="submit"/>
<!-- 								<button class="btn">Cancel</button> -->
							</div>
                    

                </fieldset>
            </form>
            </div>
            </div>
 
<div id="popup" title="Pie Chart"> 
<div id="graph" title=" Graph View  "></div>
<!--div id="graph" title=" Graph View "></div-->


<div id="inner"></div>
</div>      

</body>
</html>
