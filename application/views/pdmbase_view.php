<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <?php
 //error_reporting(0);
include_once "data.php";
 ?>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script> -->
<script src="../js/jquery-1.7.2.min.js"></script>
<script src="../js/jquery-ui-1.8.20.custom.min.js"></script>
<!-- <script src="../../../js/bootstrap-datepicker.js"></script> -->
<script type="text/javascript" src="../js/jquery.multiselect.min.js"></script>

<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/datepicker.css" rel="stylesheet">
<link href="../atim_pdm/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../atim_pdm/css/redmond/jquery-ui-1.8.20.custom.css" />  
<link rel="stylesheet" href="../atim_pdm/css/jquery.multiselect.css" />
<!-- <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" /> -->

<style type="text/css">
 .ui-corner-all input {
 	display: inline;
 	margin-top: 0px;
 } 
 
 .containerCustom {
 	margin-top: 45px;
 }
 
 h2 {
 	margin: 0px 0px 15px 70px;
 	//border-bottom: 1px black solid;
 }
 
 #search {
 	border-top: 1px solid #DDDDDD;
 	padding-top: 30px;
 }
 </style>

<script>
	$(function() {
		populateColumns();
	    $('#form_fields').change(populateColumns);
	    $("#columns").change(populateValues);
	   $('.datepicker').datepicker({ numberOfMonths: 3, 
		   changeMonth: true,
	        changeYear: true,
	        defaultDate: '-2m',
	        yearRange: '2008:'
			   });
	   $('#form_fields option:selected').removeAttr('selected');
	   //$("#form_fields").multiselect({noneSelectedText: 'Select Form(s)'});
	   //$("#columns").multiselect({noneSelectedText: 'Select Column(s)'});
	   $("#popup").hide();
	   
	});
		
   	function populateColumns() {
    	 $("#columns").html("");
         var allVals = [];
         $('#form_fields :checked').each(function() {
           allVals.push($(this).val());
         });
         
         $.getJSON("pdm/populate", {'form_ids[]': allVals, 'columns': true}, function(data) {
				
        	    $.each(data.columns, function(index, value) { 
        
            	    $("#columns").append("<option  class='clearfix' value='" + value + "'> " + value + "</option>");
        	    	
        	    }); 
        	    populateValues();
       	 });
      }

	  function populateValues() {
		  $("#values").html("");
		  var column = $("#columns").val();
		  var form_id = $("#form_fields").val();
		  $.getJSON("pdm/populate", {'column': column, 'form_id': form_id, 'values': true,'columns' : false}, function(data) {
			
      	    $.each(data.values, function(index, value) { 
          	    $("#values").append("<option  class='clearfix' value='" + value + "'> " + value + "</option>");
      	    }); 
     	 });
	  } 

    
</script>

<title>ATIM</title>
</head>
<body data-spy="scroll" data-offset="50" data-twttr-rendered="true">
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse"> <span class="icon-bar"></span>
				<span class="icon-bar"></span><span class="icon-bar"></span>
			<a class="brand" href="../">ATIM</a>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<li class=""><a href="../">Pie Charts</a>
					</li>
				</ul>
				<ul class="nav">
					<li class=""><a href="./pdmview">Point Density Map</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container containerCustom">
<div class="well">
<h2>Begin Point Density Map Query</h2>

<form class="form-horizontal" action="../extractCoordinates.php" method="post" id="search" accept-charset="UTF-8">
                <fieldset>
                    <div class="control-group check_boxes">
                        <label for="form_fields" class="check_boxes control-label">Form Field</label>
                        <div class="controls scrollable">
                        <select  id="form_fields" name="form_id" class="clearfix">
<?php
	$result = $form_id;
	foreach($result as $row){
		echo "<option  class='clearfix' value='" . $row['id'] . "'> " . $row['form_id'] . "</option>";
	}
	date_default_timezone_set('UTC');
	$currDate = date("m/d/Y");
	
 ?>
 </select>

                        </div>
                    </div>
                    <div class="control-group">
                        <label for="columns" class="control-label">Column</label>
                        <div class="controls scrollable">
                        <select  name="column" id="columns">
                        </select>

                        </div>
                    </div>
                    
                     <div class="control-group">
                        <label for="value" class="control-label">Value</label>
                        <div class="controls scrollable">
                        <select  name="value" id="values">
                        </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
						<label class="control-label" for="from">From</label>
					 	<div class="controls" id="columns">
			            	<input class="span2 datepicker" id="from" type="text" name="from" value="">
			            </div>
					</div>
					
                    <div class="control-group">
						<label class="control-label" for="to">To</label>
					 	<div class="controls" id="columns">
			            	<input class="span2 datepicker" id="to" type="text" name="to" value="">
			            </div>
					</div>
					
	  	

                    
                    <div class="form-actions">
                          <input id='submit' type="submit" class="btn btn-primary" value="Run Search" name="submit"/>

							</div>
                    

                </fieldset>
            </form>
            </div>
            </div>
 
<div id="popup" title="Pie Chart"> 
<div id="inner"></div>
</div>      
    
</body>
</html>
