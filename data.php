<?php
include_once 'database_connection.php';

function form_fields() {
	$dbconn = connect();
	$sql = "select id, form_id from dl_forms order by form_id";
	$result = $dbconn->query($sql);
	$dbconn->close();
	return $result;
}

//could not use fields column of dl_forms because we need to find distinct and order alphabetically
function column_names($form_ids) {
	$dbconn = connect();
	$ids_string = implode(",", $form_ids);
	$sql = "select distinct column_name from dl_forms_display_template where form_id in ($ids_string) order by column_name";
	error_log($sql);
	$result = $dbconn->query($sql);
	error_log($dbconn->error);
	$column_names = array();
	while($row = $result->fetch_assoc()) {
		array_push($column_names, $row["column_name"]);
	}
	error_log(implode(",", $column_names));
	return $column_names;
}

?>